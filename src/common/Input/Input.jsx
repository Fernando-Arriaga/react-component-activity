import React from 'react';

function Input(props) {
	return <input id={props.id} type='text' placeholder={props.placeholder} />;
}

export default Input;
